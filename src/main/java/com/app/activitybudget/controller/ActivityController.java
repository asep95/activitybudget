package com.app.activitybudget.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.app.activitybudget.model.Activity;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/api")
public class ActivityController {
    private List<Activity> activities = new ArrayList<>();

    @PostMapping("/activities")
    public ResponseEntity<String> createActivity(@RequestBody List<Activity> activity) {
        System.out.println("Hello World!");
        activities.addAll(activity);
        return ResponseEntity.ok("Kegiatan berhasil dibuat");
    }

    @GetMapping("/activities")
    public List<Activity> getAllActivities() {
        return activities;
    }

    @GetMapping("/activities/{id}")
    public ResponseEntity<Activity> getActivityById(@PathVariable String id) {
        for (Activity activity : activities) {
            if (activity.getId().equals(id)) {
                return ResponseEntity.ok(activity);
            }
        }
        return ResponseEntity.notFound().build();
    }

    @PutMapping("/activities/update/{id}")
    public ResponseEntity<String> updateMoney(@PathVariable String id, @RequestBody Activity activityValue) {

        for (Activity activity : activities) {
            if (activity.getId().equals(id)) {
                activity.setCurrentMoney(activityValue.getCurrentMoney());
                activity.setLastUpdated(LocalDate.now());
                return ResponseEntity.ok("Sisa uang berhasil diupdate");
            }
        }
        return ResponseEntity.notFound().build();
    }

    @GetMapping("/report")
    public ResponseEntity<String> generateReport() {
        try {
            File file = new File("report_" + LocalDate.now().toString() + ".txt");
            FileWriter writer = new FileWriter(file);

            for (Activity activity : activities) {
                writer.write("Kegiatan: " + activity.getCategory() + "\n");
                writer.write("Sisa Uang: " + activity.getCurrentMoney() + "\n");
                writer.write("Target Uang: " + activity.getTargetMoney() + "\n");
                writer.write("Tanggal Update: " + activity.getLastUpdated() + "\n");
                writer.write("\n");
            }

            writer.close();
            return ResponseEntity
                    .ok("Laporan berhasil dihasilkan dalam file report_" + LocalDate.now().toString() + ".txt");
        } catch (IOException e) {
            e.printStackTrace();
            return ResponseEntity.badRequest().body("Gagal menghasilkan laporan");
        }
    }

    @DeleteMapping("/activities/{id}")
    public ResponseEntity<String> deleteActivity(@PathVariable String id) {
        for (Activity activity : activities) {
            if (activity.getId().equals(id)) {
                activities.remove(activity);
                return ResponseEntity.ok("Kegiatan berhasil dihapus");
            }
        }
        return ResponseEntity.notFound().build();
    }
}
