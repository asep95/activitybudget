package com.app.activitybudget.model;

import java.time.LocalDate;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Activity {
    private String id;
    private String category;
    private double currentMoney;
    private double targetMoney;
    private LocalDate lastUpdated;

    public Activity() {
        this.id = java.util.UUID.randomUUID().toString();
        this.lastUpdated = LocalDate.now();
    }
}
