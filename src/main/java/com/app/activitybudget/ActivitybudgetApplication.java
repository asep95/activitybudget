package com.app.activitybudget;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ActivitybudgetApplication {

	public static void main(String[] args) {
		SpringApplication.run(ActivitybudgetApplication.class, args);
	}

}
